import javax.lang.model.util.ElementScanner14;
public class  GCDRec {

	public static void main(String[] args){
        int value1 = Integer.parseInt(args[0]);
		int value2 = Integer.parseInt(args[1]);
		System.out.println(GCD(value1, value2));
    }
	public static int GCD(int value1,int value2){
        if ( value1 != 0 || value2 !=0 ){
            if(value2==0) {
                return value1;
            }
            else if ( value1 == 0){
                return value2;
            }
            else if(value1 < value2) {
                return GCD(value1,value2 % value1);
            }
			else{
				return GCD(value2, value1 % value2);
			}
        }
        else {
            return 1;
        }
    }

}
