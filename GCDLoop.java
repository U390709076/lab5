import javax.lang.model.util.ElementScanner14;
public class  GCDLoop {

	public static void main(String[] args){
        int value1 = Integer.parseInt(args[0]);
		int value2 = Integer.parseInt(args[1]);

		while(value1!=0 && value2!=0){
			if(value1 < value2){
				value2 = value2 % value1;
			}
			else{
				value1 = value1 % value2;
			}
			if(value1<0 || value2<0){
				value1=0;
				value2=0;
				break;
			}
		}
		if (value1==0 && value2 != 0){
			System.out.println(value2);
		}
		else if (value2==0 && value1 != 0){
			System.out.println(value1);
		}
		else{
			System.out.println("1");
		}
	
    }
}
