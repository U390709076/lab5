import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

	public static void main(String[] args) throws FileNotFoundException{
		int matrix[][] = readMatrix();

		boolean found = false;
search: for (int i=0; i< matrix.length; i++) {
			for (int j=0; j < matrix[i].length; j++) {
				if (search(0,matrix, i,j,false)){
					if(search(0, matrix, i, j, true)){
						found = true;
						break search;
					}
				}
			}
		}
			
		if (found) {
			System.out.println("A sequence is found");
		}
		printMatrix(matrix);

	}

	private static boolean search (int number, int[][]matrix, int row, int col, boolean found) {
		boolean left = (row>0 && (number+1)==matrix[row-1][col]);
		boolean right = (row<8 && (number+1)==matrix[row+1][col]);
		boolean down = (col>0 && (number+1)==matrix[row][col-1]);
		boolean up = (col<8 && (number+1)==matrix[row][col+1]);
		if (number==9 && number == matrix[row][col]){
			if (found){
				matrix[row][col]=(9-number);
			}
			return true;
		}
		else if (number==matrix[row][col]){
			if(found){
				matrix[row][col]=(9-number);
			}
			number++;
			if(left){
				return (search(number, matrix, row-1, col, found));
			}
			else if(right){
				return (search(number, matrix, row+1, col, found));
			}
			else if(up){
				return search(number, matrix, row, col+1, found);
			}
			else if(down){
				return search(number, matrix, row, col-1, found);
			}
			else{
				return false;
			}
		}
		else {
			return false;
		}
	}	
	private static int[][] readMatrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("matrix.txt");

		try (Scanner sc = new Scanner(file)){

			
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
